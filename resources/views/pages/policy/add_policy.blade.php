@extends('layouts.template')
@section('content')
<div class="content-page">
        <!-- Start content -->
        <div class="content">

            <div class="container-fluid">

                <div class="row">
                    <div class="col-xl-12">
                            <div class="breadcrumb-holder">
                                <h1 class="main-title float-left card-header">Add Policies</h1>
                                <div class="clearfix"></div>
                            </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="card-body bg-white border border">
							<form action="#" method="POST">
                                <div class="form-group">
                                  <label for="policy">Policy Name *</label>
                                  <input type="text" class="form-control" name="l_policy" aria-describedby="textHelp" placeholder="Enter Your Policy" required>
                                </div>
                                <div class="form-group">
                                  <label for="date">Create Date *</label>
                                  <input type="text" class="form-control" name="datepicker" id="datepicker"  aria-describedby="textHelp" placeholder="Enter Your Date" required>
                                </div>
                                <div class="form-group">
                                  <label for="file">Choose File *</label>
								    <input type="file" name="files" class="form-control" id="filer_example2" multiple="multiple">
                                </div>
                                <button type="submit" class="btn btn-primary">Add Policy</button>
                                <button type="#" class="btn btn-danger">Back</button>
                            </form>

                        </div>
                    </div><!-- end card-->
                </div>
            </div>
            <!--end row--->
        </div>
    </div>
    <!-- END container-fluid -->
@endsection
