@extends('layouts.template')
@section('content')
<div class="content-page">
        <!-- Start content -->
        <div class="content">

            <div class="container-fluid">

                <div class="row">
                    <div class="col-xl-12">
                            <div class="breadcrumb-holder">
                                <h1 class="main-title float-left card-header">Policies Category</h1>
                                <div class="clearfix"></div>
                            </div>
                    </div>
                </div>
                <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <!--add button-->
								<a role="button" href="#" class="btn btn-primary btn-sm ml-4"><span class="p-2"><i class="fa fa-plus"></i></span>Add</a><br><br>
                            <div class="card mb-3">
                                <div class="card-body">
                                    <table id="table-body" class="table table-bordered table-responsive-xl table-hover">
                                        <thead class="bg-dark text-white">
                                            <tr>
                                                <th>No</th>
                                                <th>Policies Category</th>
                                                <th>Status</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                              <td>1</td>
                                              <td></td>
                                              <td></td>
                                                <!------------icons------------->
                                                <td>
                                                    <a href="#" class="btn btn-primary btn-sm btn-edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                    <a href="#" class="btn btn-primary btn-sm btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                    <a href="#" class="btn btn-primary btn-sm btn-success"><i class="fa fa-check" aria-hidden="true">Publish</i></a>
                                                    <a href="#" class="btn btn-primary btn-sm btn-danger"><i class="fa fa-check" aria-hidden="true">Unpublish</i></a>
                                                    </td>
                                                </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div><!-- end card-->
                        </div>
                </div>
                <!--end row--->
            </div>
        </div>
        <!-- END container-fluid -->
    </div>
@endsection
