<footer class="footer">
    <span class="text-right">
        Copyright@<a target="_blank" href="#">EHRM</a>
    </span>
    <span class="float-right">
        Powered by <a target="_blank" href="@"><b>Ezecom</b></a>
    </span>
</footer>
</div> <!--end main-->
<!-- javascript -->
<script src="{{ asset('js/modernizr.min.js')}}"></script>
<script src="{{ asset('js/jquery.min.js')}}"></script>
<script src="{{ asset('js/moment.min.js')}}"></script>
<script src="{{ asset('js/popper.min.js')}}"></script>
<script src="{{ asset('js/bootstrap.min.js')}}"></script>
<script src="{{ asset('js/detect.js')}}"></script>
<script src="{{ asset('js/fastclick.js')}}"></script>
<script src="{{ asset('js/jquery.blockUI.js')}}"></script>
<script src="{{ asset('js/jquery.nicescroll.js')}}"></script>
<script src="{{ asset('js/jquery.scrollTo.min.js')}}"></script>
<script src="{{ asset('plugins/switchery/switchery.min.js')}}"></script>
<!--ajax modal--->
<script src="{{ asset('js/ajaxscript.js')}}"></script>
<!-- App js -->
<script src="{{ asset('js/pikeadmin.js')}}"></script>
<!--filer identify size for file upload -->
<script src="{{ asset('js/jquery.filer.min.js') }}"></script>
<!---select 2--->
<script src="{{ asset('plugins/select2/js/select2.min.js')}}"></script>
<script>
    $(document).ready(function(){
            $('#select2,#edit_select2,#pro-select2,#count-select2').select2();
        });
    </script>

<!-- BEGIN Java Script -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
<!-- Counter-Up-->
<script src="plugins/waypoints/lib/jquery.waypoints.min.js"></script>
<script src="plugins/counterup/jquery.counterup.min.js"></script>
<script src="{{asset('datepicker/daterangepicker.js') }}"></script>
<script src="{{asset('datepicker/moment.min.js') }}"></script>
<script>
    $(document).ready(function(){
             // data-tables
             $('#table-body').DataTable();

            // counter-up
            $('.counter').counterUp({
                delay: 10,
                time: 600
            });

            // daterange picker
            $('input[name="datepicker"]').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true
            });

            // identify file upload
            'use-strict';
            $('#filer_example2').filer({
            // limit: 3,
            maxSize: 36,
            extensions: ['docx', 'pdf', 'xls','png'],
            changeInput: true,
            showThumbs: true,
            addMore: true
            });
        });
  </script>
</body>
</html>
