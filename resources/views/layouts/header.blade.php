<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Noodle</title>

        <!-- Important to work AJAX CSRF -->
        <meta name="csrf-token" content="{{ csrf_token() }}" />
		<!-- Favicon -->
		<link rel="shortcut icon" href="{{asset('images/ehrm.jpg ')}}">

		<!-- Bootstrap CSS -->
		<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />

		<!-- Font Awesome CSS -->
		<link href="{{('font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />

		<!-- Custom CSS -->
		<link href="{{asset('css/style.css')}}" rel="stylesheet" type="text/css"/>

		<!-- BEGIN CSS for this page -->
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css"/>
        <!-- Select 2 -->
		<link href="plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
        <!--modal js-->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
		<!-- date picker-->
        <link rel="stylesheet" href="{{asset('css/datepicker.css')}}">
        <!--daterangepicker--->
        <link rel="stylesheet" href="{{asset('datepicker/daterangepicker.css') }}">
        <!--filer--->
        <link rel="stylesheet" href="{{ asset('plugins/jquery.filer/css/jquery.filer.css') }}">

</head>
<body class="adminbody">
