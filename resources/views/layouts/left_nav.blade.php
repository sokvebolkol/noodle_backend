<!-- Left Sidebar -->
<div class="left main-sidebar">

        <div class="sidebar-inner leftscroll">

            <div id="sidebar-menu">
                <ul><br>
                    <li class="submenu">
                        <a href="#"><i class="fa fa-fw fa-book"></i> <span>Policy </span> <span class="menu-arrow"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="">Country</a></li>
                            <li><a href="">Province</a></li>
                        </ul>
                    </li>
                    <li class="submenu">
                        <a href="#"><i class="fa fa-fw fa-calendar"></i> <span>Company </span> <span class="menu-arrow"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="">Country</a></li>
                            <li><a href="">Province</a></li>
                        </ul>
                    </li>
                    <li class="submenu">
                        <a href="#"><i class="fa fa-fw fa-wpforms"></i> <span>Form </span> <span class="menu-arrow"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="">Country</a></li>
                            <li><a href="">Province</a></li>
                        </ul>
                    </li>
                    <li class="submenu">
                        <a href="#"><i class="fa fa-newspaper-o"></i> <span>News & Events </span> <span class="menu-arrow"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="">Country</a></li>
                            <li><a href="">Province</a></li>
                        </ul>
                    </li>
                    <li class="submenu">
                        <a href="#"><i class="fa fa-certificate"></i> <span>Certification </span> <span class="menu-arrow"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="">Country</a></li>
                            <li><a href="">Province</a></li>
                        </ul>
                    </li>
                    <li class="submenu">
                        <a href="#"><i class="fa fa-fw fa-calendar"></i> <span>Calendar </span> <span class="menu-arrow"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="">Country</a></li>
                            <li><a href="">Province</a></li>
                        </ul>
                    </li>
                    <li class="submenu">
                        <a href="#"><i class="fa fa-user"></i> <span>User </span> <span class="menu-arrow"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="">Country</a></li>
                            <li><a href="">Province</a></li>
                        </ul>
                    </li>
                </ul>
            <div class="clearfix"></div>

        </div>

        <div class="clearfix"></div>

    </div>
</div>
<!-- End Sidebar -->
