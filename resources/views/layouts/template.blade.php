
@include('layouts.header')
<div id="main">
        @yield('content')
<!--top menu--->
@include('layouts.top_nav')
<!--side bar left--->
@include('layouts.left_nav') 
<!--footer-->
@include('layouts.footer')
